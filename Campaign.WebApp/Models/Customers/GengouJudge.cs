﻿using System;

namespace Campaign.WebApp.Models.Customers
{
    public sealed class GengouJudge
    {
        private readonly DateTime _value;

        public GengouJudge(DateTime value)
        {
            _value = new DateTime(value.Year, value.Month, value.Day);
        }

        public string GetGengou()
        {
            System.Globalization.CultureInfo ci =
                new System.Globalization.CultureInfo("ja-JP", false);
            ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();
            return _value.ToString("gg", ci);
        }

        public bool IsHeisei()
        {
            return GetGengou() == Gengou.Heisei().ToString();
        }

    }
}
