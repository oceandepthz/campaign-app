﻿using Campaign.WebApp.Models.Campaigns;
using Campaign.WebApp.Models.Customers;
using Microsoft.Extensions.DependencyInjection;

namespace Campaign.WebApp.Models
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            return services
                .AddScoped<ICustomerService, CustomerService>()
                .AddScoped<AcceptNumberGenerator>()
                .AddScoped<IApplyCampaignService, ApplyCampaignService>();
        }
    }
}