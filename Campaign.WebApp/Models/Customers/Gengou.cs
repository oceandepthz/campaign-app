﻿namespace Campaign.WebApp.Models.Customers
{
    public class Gengou
    {
        private readonly string _value;

        private Gengou(string value)
        {
            _value = value;
        }

        public static Gengou Heisei()
        {
            return new Gengou("平成");
        }

        public override string ToString()
        {
            return _value;
        }
    }
}
