﻿using System;

namespace Campaign.WebApp.Models.Customers
{
    public sealed class BirthDate
    {
        private readonly DateTime _value;

        public BirthDate(DateTime value)
        {
            _value = new DateTime(value.Year, value.Month, value.Day);
        }

        public override string ToString()
        {
            return _value.ToString("yyyy年MM月dd日");
        }

        public DateTime ToDateTime()
        {
            return _value;
        }

        public bool IsHeisei()
        {
            GengouJudge gengouJudge = new GengouJudge(_value);
            return gengouJudge.IsHeisei();
        }
    }
}