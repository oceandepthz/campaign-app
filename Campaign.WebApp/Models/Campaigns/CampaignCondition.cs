﻿using Campaign.WebApp.Models.Customers;

namespace Campaign.WebApp.Models.Campaigns
{
    public class CampaignCondition
    {
        private readonly Gender _gender;
        private readonly BirthDate _birthDate;

        public CampaignCondition(Customer value)
        {
            _gender = value.Gender;
            _birthDate = value.BirthDate;
        }

        public bool CanCampaignApply()
        {
            return _gender == Gender.Male && _birthDate.IsHeisei();
        }
    }
}
