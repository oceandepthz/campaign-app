﻿using Campaign.WebApp.Models.Campaigns;
using Campaign.WebApp.Models.Customers;
using Microsoft.Extensions.DependencyInjection;

namespace Campaign.WebApp.Infrastructure
{
    public static class InfrastructureExtensions
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            return services
                .AddScoped<ICustomerRepository, CustomerDataSource>()
                .AddScoped<IAcceptNumberRepository, AcceptNumberRepository>()
                .AddScoped<ICampaignApplicationRepository, CampaignApplicationRepository>();
        }
    }
}