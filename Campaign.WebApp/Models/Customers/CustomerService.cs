﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Campaign.WebApp.Models.Customers
{
    internal sealed class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customers;

        public CustomerService(ICustomerRepository customers)
        {
            _customers = customers;
        }

        public IEnumerable<Customer> All()
        {
            return _customers.All().ToArray();
        }

        public Customer Find(Guid customerId)
        {
            return _customers.Find(customerId);
        }
    }
}