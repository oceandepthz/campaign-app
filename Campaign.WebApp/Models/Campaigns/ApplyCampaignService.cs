﻿using Campaign.WebApp.Models.Customers;

namespace Campaign.WebApp.Models.Campaigns
{
    internal sealed class ApplyCampaignService : IApplyCampaignService
    {
        private readonly AcceptNumberGenerator _acceptNumberGenerator;
        private readonly ICampaignApplicationRepository _applications;

        public ApplyCampaignService(AcceptNumberGenerator acceptNumberGenerator,
            ICampaignApplicationRepository applications)
        {
            _acceptNumberGenerator = acceptNumberGenerator;
            _applications = applications;
        }

        public int Apply(Customer customer)
        {
            CampaignCondition campaignCondition = new CampaignCondition(customer);
            if (!campaignCondition.CanCampaignApply())
            {
                return 0;
            }

            var acceptNumber = _acceptNumberGenerator.NewNumber();

            var application = new CampaignApplication(acceptNumber, customer.Id);

            _applications.Add(application);

            return acceptNumber;
        }
    }
}