﻿using Campaign.WebApp.Infrastructure;
using Campaign.WebApp.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Campaign.WebApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            host.ImportInitialData();

            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
        }
    }
}